<?php

namespace Drupal\reauthenticate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin settings form for reauthenticate.
 */
class ReauthenticateSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'reauthenticate.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reauthenticate_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the functionality.'),
      '#default_value' => $config->get('enable'),
    ];

    $form['timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Timeout before forcing reauthentication (in seconds)'),
      '#default_value' => $config->get('timeout') ?: 300,
    ];

    $form['max_attempt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max number of attempt before logging user out'),
      '#default_value' => $config->get('max_attempt') ?: 3,
    ];

    $form['paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages for reauthentication'),
      '#resizable' => TRUE,
      '#description' => $this->t("Specify per line the pages that would require authentication. Wildcard is allowed like '/user/*/edit*'."),
      '#default_value' => $config->get('paths'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('timeout', $form_state->getValue('timeout'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('paths', $form_state->getValue('paths'))
      ->set('enable', $form_state->getValue('enable'))
      ->set('max_attempt', $form_state->getValue('max_attempt'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
