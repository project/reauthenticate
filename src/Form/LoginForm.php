<?php

namespace Drupal\reauthenticate\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reauthenticate\EventSubscriber\ReauthenticateSubscriber;
use Drupal\user\Entity\User;

/**
 * The main form for logging in and validation.
 */
class LoginForm extends FormBase {

  public const REAUTHENTICATION_SESSION_ATTEMPTS = 'reauthentication_session_attempts';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reauthenticate_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Please reauthenticate to continue.'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password:'),
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $tempstore = \Drupal::service('tempstore.private')->get('reauthenticate');
    $password_checker = \Drupal::service('password');
    $password = $form_state->getValue('password');

    $current_user = \Drupal::currentUser();
    $user = User::load($current_user->id());
    if (!$password_checker->check($password, $user->getPassword())) {
      $attempts = $tempstore->get(LoginForm::REAUTHENTICATION_SESSION_ATTEMPTS);

      if (!$attempts) {
        $attempts = 0;
      }

      $attempts++;
      $tempstore->set(LoginForm::REAUTHENTICATION_SESSION_ATTEMPTS, $attempts);

      if ($attempts >= $this->getMaxAttempts()) {
        $this->logout($form_state);

        // Return here so that we are not redirected to back and trigger
        // validation error.
        return;
      }

      // Set error if not logged out.
      $form_state->setErrorByName('password', $this->t('Password is incorrect.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // On success set session variables.
    $tempstore = \Drupal::service('tempstore.private')->get('reauthenticate');
    $tempstore->set(LoginForm::REAUTHENTICATION_SESSION_ATTEMPTS, 0);
    $tempstore->set(ReauthenticateSubscriber::REAUTHENTICATION_SESSION_KEY, time());
  }

  /**
   * Max attempt logout.
   */
  public function logout(FormStateInterface $form_state) {
    $destination = \Drupal::request()->query->get('destination');
    \Drupal::request()->query->remove('destination');
    user_logout();
    $url = Url::fromUserInput($destination, ['query' => ['logout_reauthenticate' => 1]]);
    $form_state->setRedirectUrl($url);
  }

  /**
   * Gets the time needed for timeout.
   */
  public function getMaxAttempts() {
    $config = \Drupal::config('reauthenticate.settings');
    return $config->get('max_attempt') ?: 3;
  }

  /**
   * Cleanup session.
   */
  public static function cleanupAttempts() {
    $tempstore = \Drupal::service('tempstore.private')->get('reauthenticate');
    $tempstore->delete(LoginForm::REAUTHENTICATION_SESSION_ATTEMPTS);
  }

}
