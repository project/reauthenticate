<?php

namespace Drupal\reauthenticate\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Reauthenticate Subscriber.
 */
class ReauthenticateSubscriber implements EventSubscriberInterface {

  public const REAUTHENTICATION_SESSION_KEY = 'reauthentication_session_key';

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForReauthentication'];
    $events[KernelEvents::REQUEST][] = ['checkForReauthenticationLogout'];
    return $events;
  }

  /**
   * Check for logout message.
   */
  public function checkForReauthenticationLogout(RequestEvent $event) {
    $queryLogout = \Drupal::request()->query->get('logout_reauthenticate');
    if (!empty($queryLogout) && $queryLogout == 1) {
      \Drupal::messenger()->addMessage("You are logged out.",
        MessengerInterface::TYPE_WARNING);
    }
  }

  /**
   * Check for redirection.
   */
  public function checkForReauthentication(RequestEvent $event) {
    $user_current = \Drupal::currentUser();

    $config = \Drupal::config('reauthenticate.settings');
    $enabled = $config->get('enable');

    if (!$enabled) {
      return;
    }

    if ($user_current->hasPermission('bypass reauthenticate pages')) {
      return;
    }

    // Excluded routes.
    $excluded_routes = [
      'reauthenticate.login',
      'user.reset.login',
      'user.reset',
    ];

    // Other special bypass use case.
    if ($this->otherBypass()) {
      return;
    }

    $route_match = \Drupal::routeMatch();
    if (in_array($route_match->getRouteName(), $excluded_routes)) {
      return;
    }

    $request = \Drupal::request();
    $_url = Url::createFromRequest($request);

    // This should only check if user is anonymous.
    if (!$user_current->isAnonymous() && $this->isUrlMatched()) {
      if ($_url->access($user_current)) {
        if ($this->checkForSession()) {
          $this->redirectToLogin($event);
        }
      }
    }
  }

  /**
   * Checks other special bypass.
   */
  public function otherBypass() {
    $route_match = \Drupal::routeMatch();
    $pass_reset = \Drupal::request()->query->get('pass-reset-token');
    $user = $route_match->getParameter('user');
    $user_current = \Drupal::currentUser();
    if ($user instanceof UserInterface) {
      $user_id = $user->id();
    }

    // Bypass user reset.
    if ('entity.user.edit_form' == $route_match->getRouteName() &&
      $user_id == $user_current->id() && !empty($pass_reset)) {
      return TRUE;
    }
  }

  /**
   * Checks if url is for reauthentication.
   */
  public function isUrlMatched() {
    $match = FALSE;
    $current_path = \Drupal::service('path.current')->getPath();
    $path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
    $config = \Drupal::config('reauthenticate.settings');
    $paths = $config->get('paths');
    $paths = preg_split('/[\s]+/', $paths);

    $path_matcher = \Drupal::service('path.matcher');
    foreach ($paths as $path) {
      if ($path_matcher->matchPath($path_alias, $path)) {
        $match = TRUE;
        break;
      }
    }

    return $match;
  }

  /**
   * Gets the time needed for timeout.
   */
  public function getTimeout() {
    $config = \Drupal::config('reauthenticate.settings');
    return $config->get('timeout') ?: 300;
  }

  /**
   * Redirects to login page.
   */
  public function redirectToLogin(RequestEvent $event) {
    $url = Url::fromRoute('reauthenticate.login');
    $url_current = Url::fromRoute('<current>');
    $url->setOption('query', [
      'destination' => $url_current->toString(),
    ]);
    $response = new RedirectResponse($url->toString(), 301);
    $event->setResponse($response);
    $event->stopPropagation();
  }

  /**
   * Checks if a session exists.
   */
  public function checkForSession() {
    $go_to_login = FALSE;
    $tempstore = \Drupal::service('tempstore.private')->get('reauthenticate');

    if (empty($tempstore->get(ReauthenticateSubscriber::REAUTHENTICATION_SESSION_KEY))) {
      // Force login.
      return TRUE;
    }

    $session_time = $tempstore->get(ReauthenticateSubscriber::REAUTHENTICATION_SESSION_KEY);
    $timeout = $this->getTimeout();

    if (time() - $session_time > $timeout) {
      $go_to_login = TRUE;
    }

    return $go_to_login;
  }

  /**
   * Cleanup session.
   */
  public static function cleanupSession() {
    $tempstore = \Drupal::service('tempstore.private')->get('reauthenticate');
    $tempstore->delete(ReauthenticateSubscriber::REAUTHENTICATION_SESSION_KEY);
  }

}
