# Reauthenticate

This modules requires reauthentication of certain pages before they can be 
accessed.  The pages which require authentication are configurable.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/reauthenticate).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/reauthenticate).


## Requirements

Requies [rkr/wildcards](https://packagist.org/packages/rkr/wildcards) for simple
wildcard matching.


## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

You can configure that paths that require authentication 


## Maintainers

Current maintainers:
- Bryan Manalo [bryanmanalo](https://www.drupal.org/u/bryanmanalo)

This project has been sponsored by:
- iom-un-migration -
  UN Agency that deals with humane and orderly migration benefits migrants and
  society. 
- Visit: [IOM](https://www.iom.int/) for more information.
